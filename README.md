# node-libwnck-window-listener
> Libwnck-based window-listener as a node-module.

## Dependencies

* NodeJS
* libwnck

## Building

At first you have to install the dependencies:

```sh
npm install
```

Then you can build the module:

```sh
npm build .
```

## Usage

Example program:

```js
var libwnck_window_listener = require('./build/Release/node-libwnck-window-listener');

libwnck_window_listener.SetCallbacks(function(name, icon, xid) {
    // This function will be called when a new window has been opened.
    //     name - The name of the window
    //     icon - base64-encoded icon of the window
    //     xid - the window's xid
}, function(name, icon, xid) {
    // This function will be called when a window becomes the new active window.
    //     name - The name of the window
    //     icon - base64-encoded icon of the window
    //     xid - the window's xid
}, function(xid) {
    // This function will be called when a window has been closed.
    //     xid - the window's xid
});
```

## Release History

* Initial Commit

## Meta

Hannes Schulze ([guidedlinux.org](https://www.guidedlinux.org/)) projects@guidedlinux.org

Distributed under the GPL-3.0 license. See ``LICENSE`` for more information.

[https://bitbucket.org/guidedlinux/](https://bitbucket.org/guidedlinux/)

## Contributing

1. Fork it (<https://bitbucket.org/instantlinux/node-libwnck-window-listener>)
1. Create your feature branch (`git checkout -b feature/fooBar`)
1. Commit your changes (`git commit -am 'Add some fooBar'`)
1. Push to the branch (`git push origin feature/fooBar`)
1. Create a new Pull Request

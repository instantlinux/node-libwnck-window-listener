#define WNCK_I_KNOW_THIS_IS_UNSTABLE  1

#include <libwnck/libwnck.h>
#include <nan.h>
#include <gdk/gdkx.h>
#include <deque>
#include <X11/Xlib.h>

using namespace v8;

Nan::Callback open_cb;
Nan::Callback active_cb;
Nan::Callback closed_cb;
GThread *thread;
uv_async_t async;
uv_loop_t *loop = uv_default_loop();

struct CallbackData {
    int                 type;
    char*               name;
    char*               icon;
    unsigned long int   xid;
};

std::deque<struct CallbackData> threadQueue;


void asyncmsg(uv_async_t *handle) {
    // Iterate through the deque-elements.
    for (std::deque<struct CallbackData>::iterator i = threadQueue.begin(); i != threadQueue.end(); i++) {
        struct CallbackData callbackData = threadQueue.front();
        threadQueue.pop_front();
        if (callbackData.type == 0) { // Run "opened"-callback
            char xidstr[10];
            sprintf(xidstr, "%lu", callbackData.xid);
            Local<Value> argXid = Nan::New(xidstr).ToLocalChecked();
            Local<Value> argName = Nan::New(callbackData.name).ToLocalChecked();
            Local<Value> argIcon = Nan::New(callbackData.icon).ToLocalChecked();
            Local<Value> args[] = { argName, argIcon, argXid };
            open_cb.Call(3, args);
        } else if (callbackData.type == 1) { // Run "active window"-callback
            char xidstr[10];
            sprintf(xidstr, "%lu", callbackData.xid);
            Local<Value> argXid = Nan::New(xidstr).ToLocalChecked();
            Local<Value> argName = Nan::New(callbackData.name).ToLocalChecked();
            Local<Value> argIcon = Nan::New(callbackData.icon).ToLocalChecked();
            Local<Value> args[] = { argName, argIcon, argXid };
            active_cb.Call(3, args);
        } else if (callbackData.type == 2) { // Run "closed"-callback
            char xidstr[10];
            sprintf(xidstr, "%lu", callbackData.xid);
            Local<Value> argXid = Nan::New(xidstr).ToLocalChecked();
            closed_cb.Call(1, &argXid);
        }
    }
}

// A window has been opened
static void on_window_opened (WnckScreen *screen,
        WnckWindow *window,
        gpointer    data) {
    if (!wnck_window_is_skip_tasklist(window)) {
        // Encode the icon as a png with base64
        GdkPixbuf *icon = wnck_window_get_icon(window);
        gchar *iconbuf;
        gsize iconbufsize;
        gdk_pixbuf_save_to_buffer(icon, &iconbuf, &iconbufsize, "png", NULL, NULL);
        gchar *base64png = g_base64_encode((const guchar *) iconbuf, iconbufsize);

        WnckApplication *app = wnck_window_get_application(window);

        // Add callback data to deque
        struct CallbackData callbackData;
        callbackData.type = 0;
        callbackData.xid = wnck_window_get_xid(window);
        callbackData.name = const_cast<char*>(wnck_application_get_name(app));
        callbackData.icon = base64png;
        threadQueue.push_back(callbackData);
        // Call function to run the callback
        uv_async_send(&async);
    }
}

// A window has been closed
static void on_window_closed(WnckScreen *screen,
        WnckWindow *window,
        gpointer data) {
    if (!wnck_window_is_skip_tasklist(window)) {
        // Add callback data to deque
        struct CallbackData callbackData;
        callbackData.type = 2;
        callbackData.xid = wnck_window_get_xid(window);
        threadQueue.push_back(callbackData);
        // Call function to run the callback
        uv_async_send(&async);
    }
}

// Currently active window has changed
static void on_active_window_changed (WnckScreen *screen,
        WnckWindow *previously_active_window,
        gpointer    data) {
    WnckWindow *active_window;
    active_window = wnck_screen_get_active_window (screen);
    // Check if active window is available...
    if (active_window) {
        if (!wnck_window_is_skip_tasklist(active_window)) {
            // Encode the icon as a png with base64
            GdkPixbuf *icon = wnck_window_get_icon(active_window);
            gchar *iconbuf;
            gsize iconbufsize;
            gdk_pixbuf_save_to_buffer(icon, &iconbuf, &iconbufsize, "png", NULL, NULL);
            gchar *base64png = g_base64_encode((const guchar *) iconbuf, iconbufsize);

            WnckApplication *app = wnck_window_get_application(active_window);

            // Add callback data to deque
            struct CallbackData callbackData;
            callbackData.type = 1;
            callbackData.xid = wnck_window_get_xid(active_window);
            callbackData.name = const_cast<char*>(wnck_application_get_name(app));
            callbackData.icon = base64png;
            threadQueue.push_back(callbackData);
            // Call function to run the callback
            uv_async_send(&async);
        }
    }
}

static gpointer threadFunc(gpointer data) {
    // Initialize libwnck
    GMainLoop *loop;
    WnckScreen *screen;

    char** newargv;
    gdk_init (0, &newargv);

    loop = g_main_loop_new (NULL, FALSE);
    screen = wnck_screen_get_default ();

    // Connect signals
    g_signal_connect (screen, "window-opened", G_CALLBACK (on_window_opened), NULL);
    g_signal_connect (screen, "active-window-changed", G_CALLBACK (on_active_window_changed), NULL);
    g_signal_connect (screen, "window-closed", G_CALLBACK(on_window_closed), (gpointer) NULL);

    // Enter the loop
    g_main_loop_run (loop);

    g_main_loop_unref (loop);
}

NAN_METHOD(SetCallbacks) {
    // Set callbacks
    open_cb.Reset(info[0].As<Function>());
    active_cb.Reset(info[1].As<Function>());
    closed_cb.Reset(info[2].As<Function>());

    uv_async_init(loop, &async, asyncmsg);
    thread = g_thread_new("mainloopthread", (GThreadFunc) threadFunc, &open_cb);

    XInitThreads();
}

NAN_METHOD(FocusWindow) {
    if (info.Length() > 0) {
        if (info[0]->IsString()) {
            String::Utf8Value str(info[0]);
            const char *parameter = (const char *)(*str);
            WnckWindow *window = wnck_window_get(((unsigned)atol(parameter)));
            wnck_window_unminimize(window, gdk_x11_get_server_time(gdk_get_default_root_window()));
        }
    }
} 

NAN_METHOD(MinimizeWindow) {
    if (info.Length() > 0) {
        if (info[0]->IsString()) {
            String::Utf8Value str(info[0]);
            const char *parameter = (const char *)(*str);
            WnckWindow *window = wnck_window_get(((unsigned)atol(parameter)));
            wnck_window_minimize(window);
        }
    }
}

NAN_MODULE_INIT(RegisterModule) {
    NAN_EXPORT(target, SetCallbacks);
    NAN_EXPORT(target, FocusWindow);
    NAN_EXPORT(target, MinimizeWindow);
}

NODE_MODULE(libwnck_window_listener, RegisterModule);
{
    "variables": {
        "pkg-config": "pkg-config"
    },
    "targets": [
        {
            "target_name": "node-libwnck-window-listener",
            "sources": [ "src/node_libwnck_window_listener.cpp" ],
            "include_dirs": [
                "<!(node -e \"require('nan')\")"
            ],
            "cflags": [
                '<!@(<(pkg-config) --cflags libwnck-3.0)'
            ],
            "ldflags": [
                '<!@(<(pkg-config) --libs-only-L --libs-only-other libwnck-3.0)'
            ],
            "libraries": [
                '<!@(<(pkg-config) --libs-only-l libwnck-3.0)'
            ]
        }
    ]
}